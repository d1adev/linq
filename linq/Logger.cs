﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace linq
{
    class Logger
    {


        public int Start
        {
            get
            {
                return this._start;
            }
        }

        public int TimeElapsed
        {
            get
            {
                return this._timeElapsed;
            }
        }

        public bool NotDone
        {
            get
            {
                return this._notDone;
            }
            set
            {
                if (value == false)
                {
                    this._currentState = State.state.Done;
                }
            }
        }

        public State.state CurrentState
        {
            get
            {
                return this._currentState;
            }
            set
            {
                this._currentState = value;
            }
        }

        private int _start;

        private int _timeElapsed;

        private bool _notDone = true;

        private State.state _currentState = State.state.Idle;
        
        public void Log(string message, LogType.Kind k)
        {
            if (k == LogType.Kind.Info)
            {
                Console.ForegroundColor = ConsoleColor.Blue;
                Console.WriteLine("[{0} ms] - {1}", this._timeElapsed.ToString(), message);
            }
            else if (k == LogType.Kind.Error)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("[{0} ms] - {1}", this._timeElapsed.ToString(), message);
            }
            else if (k == LogType.Kind.None)
            {
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine("[{0} ms] - {1}", this._timeElapsed.ToString(), message);
            }
              
        }

        public void startLogging()
        {
            TaskFactory tf = new TaskFactory();
            Task logging = tf.StartNew(() =>
            {
                while (this._notDone)
                {
                    if (this._currentState == State.state.ReadingFile)
                    {
                        this.Log("Reading the file...", LogType.Kind.Info);
                        this._currentState = State.state.Idle;
                    }
                    if (this._currentState == State.state.ParsingMessages)
                    {
                        this.Log("Parsing the messages...", LogType.Kind.Info);
                        this._currentState = State.state.Idle;
                    }
                    if (this._currentState == State.state.CreatingWordDict)
                    {
                        this.Log("Creating the word dict...", LogType.Kind.Info);

                        this._currentState = State.state.Idle;
                    }
                    if (this._currentState == State.state.Computing)
                    {
                        this.Log("Computing the percentages...", LogType.Kind.Info);

                        this._currentState = State.state.Idle;
                    }
                    if (this._currentState == State.state.Rendering)
                    {
                        this.Log("Rendering...", LogType.Kind.Info);

                        this._currentState = State.state.Idle;
                    }
                    if (this._currentState == State.state.Done)
                    {
                        this.Log("Done", LogType.Kind.Info);
                        this._currentState = State.state.Idle;
                    }
                }
            });
            Task time = tf.StartNew(() =>
            {
                this._start = Environment.TickCount;
                while (logging.Status == TaskStatus.Running)
                {
                    this._timeElapsed = (Environment.TickCount - this._start);
                    Thread.Sleep(100);
                }
            });
        }
    }
}
