﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace linq
{
    class AnalyzerParams : IDisposable
    {
        private string _path;
        private bool _verboseLogging;
        private int _shownResults;
        private string _errorMessage = "";

        public AnalyzerParams(string path, bool verboseLogging = true, int shownResults = 100)
        {
            if (System.IO.File.Exists(path))
                this._path = path;
            else
                this._errorMessage += "File not found";

            this._verboseLogging = verboseLogging;

            if (shownResults > 0)
                this._shownResults = shownResults;
            else
                if(this._errorMessage != "")
                    this._errorMessage += " - Can't show less than 1 result";
                else
                    this._errorMessage += "Can't show less than 1 result";
        }

        public string Path
        {
            get
            {
                return this._path;
            }
        }

        public bool VerboseLogging 
        { 
            get 
            {
                return this._verboseLogging;
            } 
        }

        public int ShownResults
        {
            get
            {
                return this._shownResults;
            }
        }

        public string ErrorMessage
        {
            get
            {
                return this._errorMessage;
            }
        }

        public void Dispose()
        {
            this._path = null;
            this._verboseLogging = false;
            this._shownResults = -1;
            this._errorMessage = null;
        }
    }
}
