﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Data.Sql;
using System.Data;
using System.Text;
using System.Xml.Linq;
using System.Security.Cryptography;
using System.IO;
using System.Threading.Tasks;

namespace linq
{
    class Program
    { 
        static void Main(string[] args)
        {
            analyzer a = new analyzer(new AnalyzerParams("chatLog.txt", true, 100));
            a.start();
        }

        

















        // NOW IN analyzer.cs



        static void render()
        {
            List<string> content = File.ReadAllLines("chatLog.txt").ToList();
            List<string> messages = new List<string>();
            Dictionary<string, int> words = new Dictionary<string, int>();
            Dictionary<string, double> wordsPercentage = new Dictionary<string, double>();

            var notServerMessages = from line in content
                                    where !line.Contains("'Connected!'")
                                        && !line.Contains("'Disconnected!'")
                                        && !line.Contains("disconnected due inactivity!")
                                    select line.Replace("Whisper: ", "");


                foreach (string s in notServerMessages)
                {
                    if (!s.Equals(String.Empty))
                    {
                        /*if (!s.Contains("250"))
                        {
                            Console.ForegroundColor = ConsoleColor.White;
                        }
                        else
                        {
                            Console.ForegroundColor = ConsoleColor.Red;
                        }*/
                        var v = s.Substring(s.IndexOf("::") + 4, s.Length - s.IndexOf("::") - 5);

                        messages.Add(v);
                        //Console.WriteLine(v);
                    }

                }

            foreach (var s in messages)
            {
                foreach (var w in s.Split(' '))
                {
                    if (!words.ContainsKey(w))
                    {
                        words.Add(w, 1);
                    }
                    else
                    {
                        words[w] += 1;
                    }
                }

            }

            /*foreach(KeyValuePair<string, int> kvp in words.OrderBy(x => x.Value))
            {
                Console.WriteLine(kvp.Key + " - " + kvp.Value.ToString());
            }*/

            //Console.WriteLine(words.Count);


            //  100   max
            //    x   value  ==> x = (100 * value) / max  
            int max = words.Count;
            int y = 0;
            double lastPercentage = (words.OrderBy(x => x.Value).ElementAt(0).Value * 100.0)/max;
            double percent = 0.0;
            foreach (KeyValuePair<string, int> kvp in words.OrderByDescending(x => x.Value))
            {
                if (percent != lastPercentage)
                {
                    percent = ((100.0 * kvp.Value) / max);
                    wordsPercentage.Add(kvp.Key, percent);
                }
                else
                {
                    wordsPercentage.Add(kvp.Key, lastPercentage);
                }
                y++;
                Console.WriteLine(y.ToString() + " - " + wordsPercentage[kvp.Key]);
            }

            Console.WriteLine("sorting");
            var usableValues = from x in wordsPercentage.OrderByDescending(x => x.Value).Take(100) select x;
            Console.WriteLine("sorted");

            //Console.WindowWidth = usableValues.Count();
            string res = "";


            /*for (int i = 100; i > 0; i--)
            {
                if (i == 100)
                {
                    res += i.ToString() + "%- ";
                }
                else if(i >= 10)
                {
                    res += i.ToString() + "% - ";
                }
                else if (i <= 9)
                {
                    res += i.ToString() + "%  - ";
                }
                foreach (KeyValuePair<string, int> kvp in usableValues.Where(x => x.Value < i))
                {
                    //Console.ForegroundColor = ConsoleColor.Blue;
                    res += "o";
                }
                foreach (KeyValuePair<string, int> kvp in usableValues.Where(x => x.Value >= i))
                {
                    //Console.ForegroundColor = ConsoleColor.Green;
                    res += "/";
                }
                res += Environment.NewLine;
            }
            File.WriteAllText("ddddd.txt", res);*/

            res += "<table>";
            res += "<thead>";
            res += "<tr>";
            /*for (int i = usableValues.Count() - 1; i >= 0; i--)
            {
                res += "<th>";
                res += usableValues.ElementAt(i).Key;
                res += "</th>";
            }*/
            res += "</tr>";
            res += "</thead>";
            res += "<tbody>";
            int z = 0;
            int e = 0;
            for (int i = 100; i > 0; i--)
            {
                Console.WriteLine(i.ToString() + " - in the loop");
                res += "<tr>";
                /*
                if (i == 100)
                {
                    res += i.ToString() + "%-";
                }
                else if (i >= 10)
                {
                    res += i.ToString() + "%  - ";
                }
                else if (i <= 9)
                {
                    res += "0" + i.ToString() + "%  - ";
                }
             */

                foreach (KeyValuePair<string, double> kvp in usableValues.Where(x => x.Value < i))
                {
                    z++;
                    Console.WriteLine(z.ToString() + " - in the first foreach");
                    //Console.ForegroundColor = ConsoleColor.Blue;
                    res += "<th colspan=\"1\"><span class=\"o\">" + returnNumber(i) +"</span></th>";
                }

                foreach (KeyValuePair<string, double> kvp in usableValues.Where(x => x.Value >= i))
                {
                    e++;
                    Console.WriteLine(e.ToString() + " - in the second foreach");
                    //Console.ForegroundColor = ConsoleColor.Green;
                    res += "<th colspan=\"1\"><span class=\"slash\">" + returnNumber(i) + "</span></th>";
                }
                res += "</tr>";
            }
            res += "</tbody>";
            res += "</table>";
            res += "The most used word is : " + words.OrderByDescending(x => x.Value).ElementAt(0).Key + " : " + words.OrderByDescending(x => x.Value).ElementAt(0).Value + " times = " + wordsPercentage[words.OrderByDescending(x => x.Value).ElementAt(0).Key] + "%";
            res += "The least used word is : " + words.OrderBy(x => x.Value).ElementAt(0).Key + " : used " + words.OrderBy(x => x.Value).ElementAt(0).Value + " times = " + wordsPercentage[words.OrderBy(x => x.Value).ElementAt(0).Key] + "%";
            res += "<style>.o { background-color: blue; } .slash { background-color: red; } </style>";
            File.WriteAllText("index.html", res);
            System.Diagnostics.Process.Start("index.html");





            /*var connections = from line in content where line.Contains("'Connected!'") select line;
            Console.WriteLine(connections.Count());

            var disconnections = from line in content where line.Contains("'Disconnected!'") select line;
            Console.WriteLine(disconnections.Count());

            var vouchers = from line in content where line.Contains("botter") || line.Contains("bot") select line;
            Console.WriteLine(vouchers.Count());

            Dictionary<string, int> vouchAsk = new Dictionary<string, int>();
            foreach (var v in vouchers)
            {
                if (!vouchAsk.ContainsKey(v.Split('-')[1].Split('>')[0]))
                {
                    vouchAsk.Add(v.Split('-')[1].Split('>')[0], 1);
                }
                else
                {
                    vouchAsk[v.Split('-')[1].Split('>')[0]] += 1;
                }
            }

            foreach (KeyValuePair<string, int> kvp in vouchAsk.OrderBy(x => x.Value))
            {
                Console.WriteLine(kvp.Key + " - " + kvp.Value);
            }


            var takeiteasymsgs = from msgs in content where msgs.Contains("[Sabotazh]") && (msgs.Contains("bot") || msgs.Contains("botter")) select msgs;
            foreach (var v in takeiteasymsgs)
            {
                Console.WriteLine(v);
            } */
        }

        static string returnNumber(int n)
        {
            if(n == 100){
                return n.ToString();
            }
            else if(n >= 10 && n < 100){
                return "0" + n.ToString();
            }
            else if (n < 10)
            {
                return "00" + n.ToString();
            }
            else
            {
                return "wtf";
            }
        }
    }
}
