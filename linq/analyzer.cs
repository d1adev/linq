﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.IO;

namespace linq
{
    class analyzer
    {
        private List<string> content;
        private List<string> messages = new List<string>();
        private Dictionary<string, int> words = new Dictionary<string, int>();
        private Dictionary<string, double> wordsPercentage = new Dictionary<string, double>();
        private IEnumerable<KeyValuePair<string, double>> usableValues;
        private int shownResults;
        private string path;
        private bool verbose;
        private Logger logger = new Logger();

        public analyzer(AnalyzerParams prms)
        {
            if (prms.ErrorMessage != "")
            {
                logger.Log(prms.ErrorMessage + Environment.NewLine + "Press any key to exit", LogType.Kind.Error);
                logger.NotDone = false;
                Console.Read();
                Environment.Exit(0);
            }
            else
            {
                logger.NotDone = true;
                this.path = prms.Path;
                this.verbose = prms.VerboseLogging;
                this.shownResults = prms.ShownResults;
                
            }
        }

        public void start()
        {
            if (this.verbose)
            {
                logger.startLogging();
            }
            this.readFile();
            this.getUserMessages();
            this.createWordDict();
            this.computePercentage(this.shownResults);
            this.render();
        }

        private void readFile()
        {
            logger.CurrentState = State.state.ReadingFile ;
            this.content = File.ReadAllLines(this.path).ToList();
        }

        private void getUserMessages()
        {
            
                logger.CurrentState = State.state.ParsingMessages ;
            
            var notServerMessages = from line in this.content
                                    where !line.Contains("'Connected!'")
                                        && !line.Contains("'Disconnected!'")
                                        && !line.Contains("disconnected due inactivity!")
                                    select line.Replace("Whisper: ", "");

            foreach (string s in notServerMessages)
            {
                if (!s.Equals(String.Empty))
                {
                    var v = s.Substring(s.IndexOf("::") + 4, s.Length - s.IndexOf("::") - 5);
                    this.messages.Add(v);
                }

            }
        }

        private void createWordDict()
        {
            logger.CurrentState = State.state.CreatingWordDict ;
            
            foreach (var s in this.messages)
            {
                foreach (var w in s.Split(' '))
                {
                    if (!this.words.ContainsKey(w))
                    {
                        this.words.Add(w, 1);
                    }
                    else
                    {
                        this.words[w] += 1;
                    }
                }

            }
        }

        private void computePercentage(int numberOfValues)
        {    
            logger.CurrentState = State.state.Computing ;
            
            int max = this.words.Count;
            double lastPercentage = (this.words.OrderBy(x => x.Value).ElementAt(0).Value * 100.0) / max;
            double percent = 0.0;
            foreach (KeyValuePair<string, int> kvp in this.words.OrderByDescending(x => x.Value))
            {
                if (percent != lastPercentage)
                {
                    percent = ((100.0 * kvp.Value) / max);
                    this.wordsPercentage.Add(kvp.Key, percent);
                }
                else
                {
                    this.wordsPercentage.Add(kvp.Key, lastPercentage);
                }
            }

            if (this.wordsPercentage.Count < numberOfValues)
                this.usableValues = from x in this.wordsPercentage.OrderByDescending(x => x.Value).Take(100) select x;
            
            else
            {
                this.usableValues = from x in this.wordsPercentage.OrderByDescending(x => x.Value).Take(numberOfValues) select x;
            }
        }

        private void render()
        {       
            logger.CurrentState = State.state.Rendering ;
            
            string res = "";

            res += "<table>";
            res += "<tbody>";
            for (int i = 100; i > 0; i--)
            {
                res += "<tr>";

                foreach (KeyValuePair<string, double> kvp in usableValues.Where(x => x.Value < i))
                {
                    res += "<th colspan=\"1\"><span class=\"o\">" + this.returnNumber(i) + "</span></th>";
                }

                foreach (KeyValuePair<string, double> kvp in usableValues.Where(x => x.Value >= i))
                {
                    res += "<th colspan=\"1\"><span class=\"slash\">" + this.returnNumber(i) + "</span></th>";
                }
                res += "</tr>";
            }
            res += "</tbody>";
            res += "</table>";
            res += "The most used word is : " + this.words.OrderByDescending(x => x.Value).ElementAt(0).Key + " : " + this.words.OrderByDescending(x => x.Value).ElementAt(0).Value + " times = " + this.wordsPercentage[this.words.OrderByDescending(x => x.Value).ElementAt(0).Key] + "% ";
            res += "The least used word is : " + this.words.OrderBy(x => x.Value).ElementAt(0).Key + " : used " + this.words.OrderBy(x => x.Value).ElementAt(0).Value + " times = " + this.wordsPercentage[this.words.OrderBy(x => x.Value).ElementAt(0).Key] + "%";
            res += "<style>.o { background-color: blue; } .slash { background-color: red; } </style>";
            File.WriteAllText("index.html", res);
            //System.Diagnostics.Process.Start("index.html");
            logger.NotDone = false;
        }

        private string returnNumber(int n)
        {
            if (n == 100)
            {
                return n.ToString();
            }
            else if (n >= 10 && n < 100)
            {
                return "0" + n.ToString();
            }
            else if (n < 10)
            {
                return "00" + n.ToString();
            }
            else
            {
                return "wtf";
            }
        }
    }
}
