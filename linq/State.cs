﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace linq
{
    public static class State
    {
        public enum state
        {
            ReadingFile,
            ParsingMessages,
            CreatingWordDict,
            Computing,
            Rendering,
            Idle,
            Done
        };
    }
}
